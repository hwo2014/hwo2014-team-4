using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Collections.Generic;


public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));
		}
	}

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;

		send(join);

        float lastPosition = 0f;

		while((line = reader.ReadLine()) != null) {
         //   Console.WriteLine(line);
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":
                    Msg_CarPositions msg2 = JsonConvert.DeserializeObject<Msg_CarPositions>(line);
                    float throttle = 0.7f;                    

                    if (Math.Abs(msg2.data[0].angle) > 10f)
                    {
                        throttle = 0.2f;
                    }
                    else
                    {
                    }

                    Console.WriteLine(msg2.data[0].angle + " " + throttle + " v " + (msg2.data[0].piecePosition.inPieceDistance - lastPosition));
                    lastPosition = msg2.data[0].piecePosition.inPieceDistance;

                     send(new Throttle(throttle));

					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					Console.WriteLine("Race init");
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				default:
                    Console.WriteLine(msg.msgType);
					send(new Ping());
					break;
			}
		}
	}

	private void send(SendMsg msg) {
     //   Console.WriteLine(msg.ToJson().ToString());
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

class Msg_Lane
{
    public int startLaneIndex;
    public int endLaneIndex;
}

class Msg_Id
{
    public string name;
    public string color;
}

class Msg_PiecePosition
{
    public int pieceIndex;
    public float inPieceDistance;
    public Msg_Lane lane;
    public int lap;
}

class Msg_CarPosition
{
    public Msg_Id id;
    public float angle;
    public Msg_PiecePosition piecePosition;
}

class Msg_CarPositions
{
    public string msgType;
    public int gameTick;
    public string gameId;
    public List<Msg_CarPosition> data;
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}